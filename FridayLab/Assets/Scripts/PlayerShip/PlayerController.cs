﻿using System;
using System.Collections;
using System.Collections.Generic;
using Scripts;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float playerShipSpeed = 10;
        [SerializeField] private Transform shoot ;
        [SerializeField] private AudioClip gotHit;

        private Vector2 movementInput = Vector2.zero;
        public GameObject bullet;

        private float soundHit = 0.25f;
        private int playerHp = 3;
        public float Gravity = -6.0f;
        private float xMin;
        private float xMax;

        private float yMin;
        private float yMax;
        
        private float padding = 0.5f;

        private void Start()
        {
            SetupMoveBoundaries();
        }

        private void SetupMoveBoundaries()
        {
            Camera gameCamera = Camera.main;
            xMin = gameCamera.ViewportToWorldPoint(new Vector2(0,0)).x + padding;
            xMax = gameCamera.ViewportToWorldPoint(new Vector2(1,0)).x - padding;

            yMin = gameCamera.ViewportToWorldPoint(new Vector2(0,0)).y + padding;
            yMax = gameCamera.ViewportToWorldPoint(new Vector2(0,1)).y - padding;
        }
        
        
        
        private void Update()
        {
            Move();
            if (Input.GetKeyDown("space"))
            {
                GetComponent<AudioSource>().Play();
            }

            if (playerHp <= 0 )
            {
                PlayerDead();
            }
        }

        private void Move()
        {
            var inputDirection = movementInput.normalized;
            var inPutVelocity = inputDirection * playerShipSpeed;
            var blackholeForce = new Vector2(2,0);
            var gravityForce = new Vector2(0,Gravity);
            var finalVelocity = inPutVelocity + gravityForce + blackholeForce;
            
            var newXPos = transform.position.x + inPutVelocity.x * Time.deltaTime;
            var newYPos = transform.position.y + inPutVelocity.y * Time.deltaTime;
            newXPos = Mathf.Clamp(newXPos, xMin, xMax);
            newYPos = Mathf.Clamp(newYPos, yMin, yMax);
            
            //Debug.DrawRay(transform.position,inPutVelocity,Color.green);
            //Debug.DrawRay(transform.position,gravityForce,Color.red);
            //Debug.DrawRay(transform.position,finalVelocity,Color.white);
            //Debug.DrawRay(transform.position,blackholeForce,Color.yellow);
            transform.position = new Vector2(newXPos, newYPos);
            
            
        }

        public void Fire()
        {
            var Bullet = Instantiate(bullet);
            Bullet.transform.position = transform.position;
            Bullet.GetComponent<Rigidbody2D>().velocity = Vector2.up *20;
        }
        
        public void OnMove(InputAction.CallbackContext context)
        {
            movementInput = context.ReadValue<Vector2>();
        }

        
        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name =="EnemyShip(Clone)")
            {
                //Debug.Log("Check");
                playerHp--;
                maingame.hpDead--;
            }

            else if (other.name == "Ball_02(Clone)")
            {
                AudioSource.PlayClipAtPoint(gotHit,Camera.main.transform.position,soundHit);
                playerHp--;
                maingame.hpDead--;
            }
            else if (playerHp <= 0)
            {
                PlayerDead();
            }
        } 
      
        

        public void PlayerDead()
        {
            Destroy(gameObject);
        }
    }
    
    
}
