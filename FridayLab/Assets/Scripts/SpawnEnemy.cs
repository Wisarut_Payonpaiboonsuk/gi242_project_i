﻿using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using EnemyShip;
using Scripts;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    public maingame gameManager;
    public GameObject enemy;
    public float spawnRate = 2f;
    float randx;
    float nextSpawn = 0.0f;
    Vector2 whereToSpawn;

    void Update()
    {
        if (Time.time > nextSpawn && gameManager.gameStart == true&& gameManager.deSpawn==false)
        {
            nextSpawn = Time.time + spawnRate;
            randx = Random.Range(-8.4f, 8.4f);
            whereToSpawn = new Vector2(randx, transform.position.y);
            Instantiate(enemy, whereToSpawn, Quaternion.identity);

        }
    }
    
}
