﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private AudioSource audioSource;

    public List<AudioClip> clips;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound(int number)
    {
        audioSource.PlayOneShot(clips[number]);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
