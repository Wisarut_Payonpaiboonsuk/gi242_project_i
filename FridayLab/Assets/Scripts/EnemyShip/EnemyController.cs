﻿using System;
using System.Collections;
using System.Collections.Generic;
using Scripts;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private Transform playerTransform;
        //[SerializeField] private float enemyShipSpeed = 5;
        [SerializeField] private Renderer playerRenderer;
        //[SerializeField] private EnemyShip enemySpaceShip;
        [SerializeField] private AudioClip enemyDead;
        [SerializeField] private AudioClip enemyshoot;
        [SerializeField] private Renderer gamemanager; 
        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;

        public SoundManager soundManager;
        
        public int Hp = 100;
        private GameObject Player;
        public float speed = 20;
        private Renderer enemyRenderer;
        private bool dead = false;
        private float enemySound=0.25f;
        public GameObject bullet;
       
        
        //private float chasingThresholdDistance = 1.0f;

        void Awake()
        {
            speed = 3f;
            enemyRenderer = GetComponent<Renderer>();
            /*var target = GameObject.Find("PlayerShip(Clone)");
            playerTransform = target.GetComponent<Transform>();
            playerRenderer = target.GetComponent<SpriteRenderer>();*/

        }

        void Update()
        {
            //MoveToPlayer();
            GoDown();
            EnemyFire();
        }

        private void OnDrawGizmos()
        {
            CollisonDebug();
        }

        private void CollisonDebug()
        {
            if (enemyRenderer != null && playerRenderer != null)
            {
                if (insectAABB(enemyRenderer.bounds,playerRenderer.bounds))
                {
                    Gizmos.color = Color.red;
                }
                else
                {
                    Gizmos.color = Color.white;
                }
                
                Gizmos.DrawWireCube(enemyRenderer.bounds.center, (2 * enemyRenderer.bounds.extents));
                Gizmos.DrawWireCube(playerRenderer.bounds.center, (2 * playerRenderer.bounds.extents));
            }
        }

        private bool insectAABB(Bounds a,Bounds b)
        {
            return (a.min.x <= b.min.x && a.max.x >= b.min.x) && (a.min.y <= b.min.y && a.max.y >= b.min.y);
        }

        private void MoveToPlayer()
        {
         
                /*Vector3 enemyPosition = transform.position;
                Vector3 playerPosition = playerTransform.position;
                enemyPosition.z = playerPosition.z; // ensure there is no 3D rotation by aligning Z position
                
                Vector3 vectorToTarget = playerPosition - enemyPosition; // vector from this object towards the target location
                Vector3 directionToTarget = vectorToTarget.normalized;
                Vector3 velocity = directionToTarget * enemyShipSpeed;
                           
                float distanceToTarget = vectorToTarget.magnitude;
    
                if (distanceToTarget > chasingThresholdDistance)
                {
                    transform.Translate(velocity * Time.deltaTime);
                }*/
            
        }        
        public void EnemyFire()
        {            
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                var Bullet = Instantiate(bullet);
                Bullet.transform.position = transform.position;
                Bullet.GetComponent<Rigidbody2D>().velocity = Vector2.down * 20;
                AudioSource.PlayClipAtPoint(enemyshoot,Camera.main.transform.position,enemySound);
                fireCounter = 0;
            }
        }

        public void GoDown()
        {
            transform.position += Vector3.down *speed* Time.deltaTime;
            
            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
            if (transform.position.y < min.y)
            {
                Destroy(gameObject);
            }
        }
        public void Takehit(int damage)
        {
            Hp -= damage;
            if (Hp <= 0)
            {
                Dead();
            }
            
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "PlayerShip(Clone)")
            {
                AudioSource.PlayClipAtPoint(enemyDead,Camera.main.transform.position,enemySound);
                Destroy(gameObject);
            }
        }

        private void Dead()
        {
            if (dead == false)
            {
                dead = true;
                Destroy(gameObject);
                maingame.score += 25;
                maingame.enemyDead = 1;
            }
        }

    }    
}

