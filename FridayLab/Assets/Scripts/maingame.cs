﻿using System;
using System.Collections;
using System.Collections.Generic;
using EnemyShip;
using PlayerShip;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Scripts
{

    public class maingame : MonoBehaviour
    {
        [SerializeField] private GameObject enemyShip;
        [SerializeField] private GameObject playerShip;
        [SerializeField] private GameObject objbullet;
        [SerializeField] private  GameObject menugame;
        [SerializeField] private  GameObject endgame;
        [SerializeField] public Text gamescore;
        [SerializeField] public Text hpscore;
        [SerializeField] private Text endscore;


        public int allEnemy = 8;
        public static int enemyDead = 0;
        public Boolean gameStart = false;
        public Text youLose;
        public Boolean deSpawn = false;

        public static int score;
        public static int hpDead = 3;
       
        private Text myscroeText;


        public void Awake()
        {
            endgame.SetActive(false);
            enemyDead = 0;
            score = 0;
            hpDead = 3;
        }

        public void StartGame()
        {
            gameStart = true;
            var newplayer = Instantiate(playerShip);
            //var newenemy = Instantiate(enemyShip);
            menugame.SetActive(false);
            newplayer.GetComponent<PlayerController>().bullet = objbullet;
        }

        public void Update()
        {
            gamescore.text = $"Score : {score}";
            hpscore.text = $"Hp : {hpDead}";
            CheckenemyDead();
            CheckPlayerDead();

        }

        public  void CheckPlayerDead()
        {
            if (hpDead == 0)
            {
                EndGame();
            }
        }

        public void CheckenemyDead()
        {
            if (enemyDead == allEnemy)
            {
                EndGame();
            }
        }
        public void EndGame()
        {
            deSpawn = true;
            youLose.text = "You Lose";
            endgame.SetActive(true);
            endscore.text = $"Score : {score}";
            //Time.timeScale = 0f;
        }

        public void NextLevel()
        {
            SceneManager.LoadScene("Game 1");
        }
        public void Restart()
        {
            SceneManager.LoadScene("Game");
        }
        
        public void Quit()
        {
            Application.Quit();
        }
    }
}