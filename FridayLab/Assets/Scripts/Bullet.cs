﻿using System;
using System.Collections;
using System.Collections.Generic;
using EnemyShip;
using UnityEngine;
using Object = UnityEngine.Object;

public class Bullet : MonoBehaviour
{
   private int Damage = 25;
   
   public void OnTriggerEnter2D(Collider2D other)
   {
      var target = other.gameObject.GetComponent<EnemyController>();
      target?.Takehit(Damage);
      if (other.name == "EnemyShip(Clone)")
      {
          Destroy(gameObject);
      }
      if (other.name == "Bullet(Clone)")
      { 
          Destroy(gameObject);
      }
      if (other.name == "DestroyBullet")
      { 
          Destroy(gameObject);
      }
   }
   
}
